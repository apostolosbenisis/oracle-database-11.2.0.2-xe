# Oracle Database on Docker
The docker build files for the Oracle Database 11g Release 2 (11.2.0.2) Express Edition container image that can be found on [docker hub](https://hub.docker.com/r/apostolosbenisis/oracle-database-11.2.0.2-xe/)

## Prerequisites
* Docker [18.06.1-ce](https://github.com/docker/docker-ce/releases/tag/v18.06.1-ce)

## How to build and run
To assist in building the images, you can use the [buildDockerImage.sh](buildDockerImage.sh) script.

The `buildDockerImage.sh` script is just a utility shell script that is an easy way to get started. Expert users are welcome to directly call `docker build` with their preferred set of parameters.

### Building the Docker Image
**IMPORTANT:** You will have to provide the installation binaries of the Oracle Database and put them into this folder.
The binaries can be downloaded from the [Oracle Technology Network](http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html),
make sure you use the Oracle Database Express Edition 11g Release 2 for Linux x64: oracle-xe-11.2.0-1.0.x86_64.rpm.zip. You also have to make sure to have internet connectivity for yum. Note that you must not uncompress the binaries. The script will handle that for you and fail if you uncompress them manually!

Before you build the image make sure that you have provided the installation binaries and put them into this folder and run the **buildDockerImage.sh** script:

    $ ./buildDockerImage.sh   


**IMPORTANT:** The resulting images will be an image with the Oracle binaries installed. The image will also create an `oracle` user with `uid=1973` and a `dba` group with the `guid=1973`. Use the `--build-arg` flag to override the `uid` and `gid` values.

    $ docker build --build-arg ORACLE_USERID=<custom oracle user id> --build-arg DBA_GROUPID=<custom dba group id> -t <image name:tag> -f Dockerfile .


You may extend the image with your own Dockerfile and create the users and tablespaces that you may need. The database supports only UTF-8.

#### Running Oracle Database Express Edition in a Docker container
To run your Oracle Database Express Edition Docker image use the **docker run** command as follows:

	docker run --name <container name> \
	--shm-size=1g \
	-p 1521:1521 -p 8080:8080 \
	-e ORACLE_PWD=<your database passwords> \
	-v [<host mount point>:]/u01/app/oracle/oradata \
	apostolosbenisis/oracle11g-11.2.0.2-xe

	Parameters:
	   --name:        The name of the container (default: auto generated)
	   --shm-size:    Amount of Linux shared memory
	   -p:            The port mapping of the host port to the container port.
	                  Two ports are exposed: 1521 (Oracle Listener), 8080 (APEX)
	   -e ORACLE_PWD: The Oracle Database SYS, SYSTEM and PDB_ADMIN password (default: auto generated)

	   -v /u01/app/oracle/oradata
	                  The data volume to use for the database.
	                  Has to be writable by the Unix "oracle" (uid: 1973) user inside the container!
	                  IMPORTANT: If omitted the database will not be persisted over container recreation.
	   -v /u01/app/oracle/scripts/startup | /docker-entrypoint-initdb.d
	                  Optional: A volume with custom scripts to be run after database startup.
	                  For further details see the "Running scripts after setup and on startup" section below.
	   -v /u01/app/oracle/scripts/setup | /docker-entrypoint-initdb.d
	                  Optional: A volume with custom scripts to be run after database startup.
	                  For further details see the "Running scripts after setup and on startup" section below.

**IMPORTANT:** On the host the user with uid:1973 (the default uid of oracle user inside the container) must have read write permission for the host mount point of the database (`-v <host mount point>:/u01/app/oracle/oradata`). It is recommended to create on the host, the `oracle` user and the `dba` group and to give them ownership of the mount point of the database. For example assuming the default uid and gid (1973) are used for the oracle user and dba group and the database is mounted on host directory `/data/oracle-volume`:

    $ sudo groupadd -g 1973 dba
    $ sudo useradd -u 1973 -g dba -d /home/oracle -s /bin/bash oracle
    $ mkdir /data/oracle-volume
    $ sudo chown -R oracle:dba /data/oracle-volume/
    $ docker run --restart unless-stopped \
    --name oracle-xe \
    --shm-size=1g \
    -p 1521:1521 -p 8080:8080 \
    -e ORACLE_PWD=oracle \
    -v /data/oracle-volume:/u01/app/oracle/oradata \
    apostolosbenisis/oracle-database-11.2.0.2-xe

On first startup of the container a new database will be created, the following lines highlight when the database is ready to be used:

    #########################
    DATABASE IS READY TO USE!
    #########################

There are two ports that are exposed in the image:
* 1521 which is the port to connect to the Oracle Database.
* 8080 which is the port of Oracle Application Express (APEX).

On the first startup of the container a random password will be generated for the database if not provided. You can find this password in the output line:

	ORACLE PASSWORD FOR SYS AND SYSTEM:

The password for those accounts can be changed via the **docker exec** command. **Note**, the container has to be running:
	docker exec oraclexe /u01/app/oracle/setPassword.sh <your password>

Once the container has been started you can connect to it just like to any other database:

	sqlplus sys/<your password>@//localhost:1521/XE as sysdba
	sqlplus system/<your password>@//localhost:1521/XE

#### Health check
The image is configured so that containers based on this image will be regularly checked for their health. The script that is called to check the health of the container is `checkHealth.sh` and is located in `/u01/app/oracle/`. Up to date, the currently latest version of docker [18.06.1-ce](https://github.com/docker/docker-ce/releases/tag/v18.06.1-ce) does not support automatic restarts based on health status.

### Running scripts after setup and on startup
The docker images can be configured to run scripts after setup and on startup. Currently `sh` and `sql` extensions are supported.
For post-setup scripts just mount the volume `/u01/app/oracle/scripts/setup` or extend the image to include scripts in this directory.
For post-startup scripts just mount the volume `/u01/app/oracle/scripts/startup` or extend the image to include scripts in this directory.
Both of those locations are also represented under the symbolic link `/docker-entrypoint-initdb.d`. This is done to provide
synergy with other database Docker images. The user is free to decide whether he wants to put his setup and startup scripts
under `/u01/app/oracle/scripts` or `/docker-entrypoint-initdb.d`.

After the database is setup and/or started the scripts in those folders will be executed against the database in the container.
SQL scripts will be executed as sysdba, shell scripts will be executed as the current user. To ensure proper order it is
recommended to prefix your scripts with a number. For example `01_users.sql`, `02_permissions.sql`, etc.

**Note:** The startup scripts will also be executed after the first time database setup is complete.    

## Further information
For more information about Oracle Database please see the [Oracle Database Online Documentation](https://docs.oracle.com/en/database/oracle/oracle-database/index.html).

## License
To download and run Oracle Database, regardless whether inside or outside a Docker container, you must download the binaries from the Oracle website and accept the license indicated at that page.

All scripts and files hosted in this project and repository required to build the Docker images are, unless otherwise noted, released under [UPL 1.0](https://oss.oracle.com/licenses/upl/) license.
