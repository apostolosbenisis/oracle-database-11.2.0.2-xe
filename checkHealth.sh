#!/bin/bash
# LICENSE
# -------
# Copyright 2018 Apostolos Benisis
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# --------

###### Global variables ######
RETURN_SUCCESS=0
RETURN_FAILED_SQL_EXECUTION=1
RETURN_FAILED_ORACLE_PROCESSE_CHECK=2
RETURN_FAILED_ORACLE_INSTANCE_STATUS=3
RETURN_FAILED_ORACLE_DB_OPEN_MODE=4
RETURN_FAILED_ORACLE_LISTENER=5

###### Check Oracle Process ######
function checkOracleProcess {
	printf "Check whether the Oracle Process run or not:"
	if ps -ef | grep --quiet [p]mon ; then
		printf "PASS\n"
		return $RETURN_SUCCESS
	else
		printf "FAIL: Oracle Process is not running\n"
		return $RETURN_FAILED_ORACLE_PROCESSE_CHECK
	fi
}

###### Check Instance Status ######
function checkInstanceStatus {
	printf "Check the instance status:"
	local OPEN_RETURN="OPEN"
	# Check Oracle DB status and store it in status
	status=`su -p oracle -c "sqlplus -s / as sysdba" << EOF
	set heading off;
	set pagesize 0;
	select status from v\\$instance;
	exit;
EOF`

	# Store return code from SQL*Plus
	ret=$?

	# SQL Plus execution was successful and database is open
	if [ $ret -eq 0 ] && [ "$status" = "$OPEN_RETURN" ]; then
		printf "PASS\n"
		return $RETURN_SUCCESS
	# Database is not open
	elif [ "$status" != "$OPEN_RETURN" ]; then
	   	printf "FAIL: Instance status %s != %s\n" $status $OPEN_RETURN
	   	return $RETURN_FAILED_ORACLE_INSTANCE_STATUS
	# SQL Plus execution failed
	else
	   	printf "FAIL: SQLplus execution failed\n"
	  	return $RETURN_FAILED_SQL_EXECUTION
	fi;
}

###### Check Database Open Mode ######
function checkDBisReadWrite {
	printf "Check whether the database can be read or written:"
	local READWRITE_RETURN="READ WRITE"
	# Check Oracle DB status and store it in status
	open_mode=`su -p oracle -c "sqlplus -s / as sysdba" << EOF
	set heading off;
	set pagesize 0;
	select open_mode from v\\$database;
	exit;
EOF`

	# Store return code from SQL*Plus
	local ret=$?

	# SQL Plus execution was successful and database is open
	if [ $ret -eq 0 ] && [ "$open_mode" = "$READWRITE_RETURN" ]; then
		printf "PASS\n"
		return $RETURN_SUCCESS
	# Database is not open
	elif [ "$status" != "$READWRITE_RETURN" ]; then
	   	printf "FAIL: Instance status %s != %s\n" $open_mode $READWRITE_RETURN
	   	return $RETURN_FAILED_ORACLE_DB_OPEN_MODE
	# SQL Plus execution failed
	else
		printf "FAIL: SQLplus execution failed\n"
	   	return $RETURN_FAILED_SQL_EXECUTION
	fi;
}

###### Check Listener ######
function checkListener {
	printf "Check whether the listener is available or not:"
	local ORACLESID="`grep $ORACLE_HOME /etc/oratab | cut -d: -f1`"
	if lsnrctl status | grep --quiet "Instance \"$ORACLESID\", status READY, has 1 handler(s) for this service..." ; then
		printf "PASS\n"
		return $RETURN_SUCCESS;
	else
		printf "FAIL: Oracle Listener is down\n"
		return $RETURN_FAILED_ORACLE_LISTENER;
	fi
}

###### MAIN ######
echo "--------------"
echo "Liveness check"
echo "--------------"
checkOracleProcess; 	rval=$?; if [ $rval -ne 0 ]; then exit $rval; fi
checkInstanceStatus; 	rval=$?; if [ $rval -ne 0 ]; then exit $rval; fi
checkDBisReadWrite; 	rval=$?; if [ $rval -ne 0 ]; then exit $rval; fi
checkListener; 			rval=$?; if [ $rval -ne 0 ]; then exit $rval; fi
